import {useState, useEffect, useContext} from 'react';
import { Form, Button } from 'react-bootstrap';
// import { useNavigate } from 'react-router-dom';
import {Navigate} from 'react-router-dom';

import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function LoginUser() {

	// Allows us to consume the user Context object and properties to use for user validation
	const {user, setUser} = useContext(UserContext);

	// State hooks to store the values of the input fields 
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	// state to determine wether submit button is enabled or not 
	const [isActive, setIsActive] = useState(false);

	// hook returns a function that lets us navigate to components
	// const navigate = useNavigate();

	console.log(email);
	console.log(password);

	function LoginUser(e) {
		// Prevents page redirection via form submission
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(typeof data.access !== "undefined") {
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
				    title: "Login Successful",
				    icon: "success",
				    text: "Welcome to Zuitt!"
				})
			} else {
				    Swal.fire({
				    title: "Authentication Failed",
				    icon: "error",
				    text: "Please, check your login details and try again."
				})


			};
		});


		// set the email of the authenticated user in the local storage
		// localStorage.setItem('email', email);
		// // sets the global user state to have properties obtained from the local storage
		// setUser({email: localStorage.getItem('email')});



		// Clear input fields after submission
		setEmail('');
		setPassword('');
		// navigate('/');

	// alert('You are now logged in.');

	};

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			// Changes the global 'user' state to store the 'id' and the 'isAdmin' property of the user which will be used for validation accross the whole application
			setUser({
				id: data._id,
				isAdmin: data.isAdmin

			})
		}) 
	};



	useEffect(() => {
		if (email !== '' && password !=='') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])

  return (

  	(user.id !== null) ? 

  	<Navigate to="/courses" />
  	:
    <Form onSubmit={(e) => LoginUser(e)}>
      <Form.Group className="mb-3" controlId="formBasicEmail">
      	<h2>Login</h2>
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email" value={email} onChange = {e => setEmail(e.target.value)} required/>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" onChange = {e => setPassword(e.target.value)} required/>
      </Form.Group>
      
      {isActive ? 
      	 <Button variant="primary" type="submit">
        Login
      </Button>
      :
       <Button variant="secondary" type="submit">
        Login
      </Button>

      }

     
    </Form>
  );
}
